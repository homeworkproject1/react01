import React, { useState } from "react";
import PropTypes from 'prop-types'
import './Authform.css';

async function LoginUser(credentials) {
    return fetch('http://localhost:3050/api/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    })
    .then(data=>data.json())
}

function AuthForm()
{
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    
    return(
        <div className="login-form">
            <h1>Login page</h1>
            <form>
                <label>
                    <p>Login:</p>
                    <input type="text" onChange={e=>setUserName(e.target.value)} required />
                </label>
                <label>
                    <p>Password:</p>
                    <input type="password" onChange={e=>setPassword(e.target.value)} required/>
                </label>
                <div>
                    <p>
                        <button type="submit" onClick={()=>LoginUser(username, password)}>Enter</button>
                    </p>
                </div>
                
            </form>
            
        </div>
    )
}

AuthForm.protoTypes = {
    setToken: PropTypes.func.isRequired
}

export default AuthForm